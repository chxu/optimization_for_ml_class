import pickle
import argparse
import os
import networkx as nx
import random
from torch.utils.data import DataLoader
import torch.optim as optim

from utils.metrics import *
from models import *
from datasets import *
from client import *
from learner import *


INDICES_PATH = "data/indices"
MNIST_PATH = "data/mnist"
random.seed(123)
seedlist = [random.randint(0,10000) for i in range(100)]

def designed_topology(size=10, degree=3):
    while True:
        topology = nx.random_regular_graph(degree, size, seed=seedlist.pop())
        if nx.is_connected(topology):
            break
    return topology

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--n_rounds',
        help='number of communication rounds;',
        type=int)
    parser.add_argument(
        '--lr',
        help='number of clients;',
        type=float,
        default=1e-2)
    parser.add_argument(
        '--local_epochs',
        help='number of local epochs;',
        type=int,
        default=1)
    parser.add_argument(
        '--local_rank',
        type=int)
    parser.add_argument(
        '--degree',
        type=int,
        default=3)
    parser.add_argument(
        '--seed',
        help='seed for the random processes;',
        type=int,
        default=1234,
        required=False)
    return parser.parse_args()


def main():
    args = parse_args()

    dist.init_process_group(backend='gloo', init_method="env://")

    rank = dist.get_rank()
    model = LinearModel()
    criterion = nn.CrossEntropyLoss()
    metric = accuracy
    device = torch.device("cpu")
    topology = designed_topology(size=dist.get_world_size(), degree=args.degree)

    optimizer = optim.SGD([param for param in model.parameters() if param.requires_grad],
                          lr=args.lr,
                          momentum=0.9,
                          weight_decay=5e-4)

    learner = Learner(model=model,
                      criterion=criterion,
                      metric=metric,
                      device=device,
                      optimizer=optimizer)

    indices_path = os.path.join(INDICES_PATH, f"client_{rank+1}.pkl")
    with open(indices_path, "rb") as f:
        indices = pickle.load(f)

    # Find a way to get world size
    client_weight = 1 / (dist.get_world_size())

    dataset = SubMNIST(MNIST_PATH, indices)
    loader = DataLoader(dataset, shuffle=True, batch_size=32)

    #Let the rank 0 process to evaluate the model performance
    if rank == 0:
        # create test loader for MNIST dataset
        transform = Compose([
            ToTensor(),
            Normalize((0.1307,), (0.3081,))
        ])
        dataset = FashionMNIST(MNIST_PATH, train=False, transform=transform)
        test_loader = DataLoader(dataset, shuffle=False, batch_size=128)
        client = Client(learner, loader, client_weight, topology, local_epochs=1, device=device, test_loader=test_loader)
    else:
        client = Client(learner, loader, client_weight, topology, local_epochs=1, device=device)

    client.run(n_rounds=args.n_rounds)


if __name__ == "__main__":
    main()
