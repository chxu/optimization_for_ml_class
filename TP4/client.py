import torch
import torch.distributed as dist


class Client(object):
    r"""Implement client

    Attributes
    ----------
    learner
    loader
    device
    local_epochs
    client_weight
    rank
    world_size

    Methods
    ----------
    __init__
    push_model
    pull_model
    run

    """

    def __init__(self, learner, loader, client_weight, topology, local_epochs=1, device=None, test_loader=None, log_freq=5):
        self.learner = learner
        self.loader = loader
        self.client_weight = client_weight
        self.local_epochs = local_epochs
        self.topology = topology
        self.test_loader = test_loader
        self.log_freq = log_freq

        if device is None:
            self.device = torch.device("cpu")

        self.device = device

        self.rank = dist.get_rank()
        self.world_size = dist.get_world_size()

    def consensus_update(self):
        for param in self.learner.model.parameters():
            param_reduce = torch.zeros(param.shape)
            for r in range(self.world_size):
                group_of_broadcast_for_r = list(self.topology.neighbors(r)) + [int(r)]
                group = dist.new_group(group_of_broadcast_for_r)
                if self.rank in group_of_broadcast_for_r:
                    param_to_transfer = param.data.clone()
                    dist.broadcast(param_to_transfer, src=r, group=group)
                    if self.rank != r:
                        param_reduce += param_to_transfer
            param_reduce = (param_reduce + param) / (len(list(self.topology.neighbors(self.rank)))+1)
            param.data = param_reduce.clone()


    def run(self, n_rounds):
        for round_idx in range(n_rounds):
            self.learner.fit_epochs(iterator=self.loader, n_epochs=self.local_epochs)
            self.consensus_update()

            if self.rank ==0 and round_idx % self.log_freq == self.log_freq-1:
                loss, acc = self.learner.evaluate_iterator(self.test_loader)
                print(f"Round {round_idx} | Loss: {loss:.3f} |Acc.: {(acc*100):.3f}%")
