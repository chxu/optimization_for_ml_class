import torch
import torch.distributed as dist
import argparse
import networkx as nx
import random
random.seed(123)
seedlist = [random.randint(0, 10000) for i in range(100)]


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--func', type=str, help='choose the function to execute')
    parser.add_argument('--local_rank', type=int)
    parser.add_argument('--T', type=int, default=10)
    parser.add_argument('--degree', type=int, default=3)
    args = parser.parse_args()
    return args

def designed_topology(size=10, degree=3):
    while True:
        topology = nx.random_regular_graph(degree, size, seed=seedlist.pop())
        if nx.is_connected(topology):
            break
    return topology


def Consensus_by_broadcast_conflit(degree=3, iterations=10):
    rank = dist.get_rank()
    size = dist.get_world_size()
    tensor_initial = torch.tensor(rank + 1, dtype=torch.float32)
    topology = designed_topology(size=size, degree=degree)
    neighbors = list(topology.neighbors(rank))
    print(f"Rank {rank} with value {tensor_initial.item()} has neighbours {neighbors}")

    for t in range(iterations):
        tensor_reduce = torch.zeros(1, dtype=torch.float32)
        group_of_broadcast_for_r = list(topology.neighbors(rank)) + [int(rank)]
        print(f"rank {rank} starts building the group")
        group = dist.new_group(group_of_broadcast_for_r) #Rank 0 : dist.new_group([0,1,2,3])
        print(f"rank {rank} builds the group")           #Rank 1: dist.new_group([0,1,2,3)]
        tensor_to_transfer = tensor_initial.clone()      #Rank 4: dist.new_group([0,1,2,3)]
        dist.broadcast(tensor_to_transfer, src=rank, group=group)
    print(f"Rank {rank} has value {tensor_reduce.item():.2f}")


def Consensus_by_broadcast(degree=3, iterations=10):
    rank = dist.get_rank()
    size = dist.get_world_size()
    tensor_initial = torch.tensor(rank + 1, dtype=torch.float32)
    topology = designed_topology(size=size, degree=degree)
    neighbors = list(topology.neighbors(rank))
    print(f"Rank {rank} with value {tensor_initial.item()} has neighbours {neighbors}")

    for t in range(iterations):
        tensor_reduce = torch.zeros(1, dtype=torch.float32)
        for r in range(size):
            group_of_broadcast_for_r = list(topology.neighbors(r)) + [int(r)]
            group = dist.new_group(group_of_broadcast_for_r)  #Rank 0: dist.new_group(neighbours of 0+[0])
            if rank in group_of_broadcast_for_r:              #Rank 1: dist.new_group(neighbours of 0+[0])
                tensor_to_transfer = tensor_initial.clone()
                dist.broadcast(tensor_to_transfer, src=r, group=group)
                if rank != r:
                    tensor_reduce += tensor_to_transfer

        tensor_reduce = (tensor_reduce + tensor_initial) / (len(neighbors) + 1)
        tensor_initial = tensor_reduce.clone()
    print(f"Rank {rank} has value {tensor_reduce.item():.2f}")


def Consensus_by_reduce(degree=3, iterations=10):
    rank = dist.get_rank()
    size = dist.get_world_size()
    tensor_initial = torch.tensor(rank + 1, dtype=torch.float32)
    topology = designed_topology(size=size, degree=degree)
    neighbors = list(topology.neighbors(rank))
    print(f"Rank {rank} has neighbours {neighbors}")

    for t in range(iterations):
        tensor_reduce = torch.zeros(1, dtype=torch.float32)
        for r in range(size):
            group_of_reduce_for_r = list(topology.neighbors(r)) + [int(r)]
            group = dist.new_group(group_of_reduce_for_r)
            if rank in group_of_reduce_for_r:
                tensor_to_transfer = tensor_initial.clone()
                dist.reduce(tensor_to_transfer, dst=r, group=group)
                if rank == r:
                    tensor_reduce = tensor_to_transfer / len(group_of_reduce_for_r)
        tensor_initial = tensor_reduce.clone()
    print(f"Rank {rank} has value {tensor_reduce.item():.2f}")

if __name__== '__main__':
    args = parse()
    function_mapping = {'broadcast':Consensus_by_broadcast, 'reduce':Consensus_by_reduce, 'broadcast_conflit':Consensus_by_broadcast_conflit}
    dist.init_process_group(backend='gloo', init_method='env://')
    function_mapping[args.func](degree=args.degree, iterations=args.T)