To launch the program:
1. Go to the virtual environment
2. `conda install networkx`
3. `python -m torch.distributed.launch --nproc_per_node=6 main.py --func broadcast --T 10 --degree 3`
