### To launch the program:
1. Go to data directory, run `python generate_data.py --n_clients 10`
2. Run `python -m torch.distributed.launch --nproc_per_node=10 --master_port=1234 main.py --n_rounds=100 --local_epochs 1 --lr 1e-3 --degree 2`

### TCP connection error: ports have been used
To check the processes launching by your program:
1. `ps aux | grep main.py | grep -v grep | awk '{print $2}'`

To kill the processes:
1. `kill $(ps aux | grep main.py | grep -v grep | awk '{print $2}')`

### TCP Runtime timeout error:
1. `ifconfig`
2. `export GLOO_SOCKET_IFNAME="lo"`

[lo is the loopback interface. This is a special network interface that the system uses to communicate with itself]