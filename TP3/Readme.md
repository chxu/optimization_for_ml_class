To launch the program:
1. Go to data directory, run python generate_data.py --n_clients 10
2. Run python -m torch.distributed.launch --nproc_per_node=11 --master_port=1234 main.py --n_rounds=100 --local_epochs 1
3. Run python -m torch.distributed.launch --nproc_per_node=11 --master_port=1234 main.py --n_rounds=50 --local_epochs 2
4. Run python -m torch.distributed.launch --nproc_per_node=11 --master_port=1234 main.py --n_rounds=25 --local_epochs 4
5. Run python -m torch.distributed.launch --nproc_per_node=11 --master_port=1234 main.py --n_rounds=10 --local_epochs 10
